# GeneTech

Adds highly genetically modified potatoes to the game. The function of these potatoes is to collect minerals from the surrounding earth and to concentrate them in the tuber.

* EisenMann        -> Steel
* BlueHope         -> Plasteel
* SilverWing       -> Silver
* GoldenAge        -> Gold
* GolowingFuture   -> Uranium
